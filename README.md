# Ohos-Week-View

#### 项目介绍
- 项目名称：Ohos-Week-View
- 所属系列：openharmony的第三方组件适配移植
- 功能：用于在应用程序中显示日历（周视图或日视图），它支持自定义样式
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：release 1.2.6

#### 效果演示

![效果演示](./screen/week-view.gif "截图")

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ohos-week-view:1.2.7')
    ......  
 }
 ```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
将自定义view添加到XML中，使用方法如下:
``` 
<com.alamkanak.weekview.WeekView
	ohos:id="$+id:weekView"
	ohos:width="match_parent"
	ohos:height="match_parent"
	app:eventTextColor="#ffffffff"
	app:textSize="35"
	app:eventTextSize = "34"
	app:hourHeight="205"
	app:headerColumnPadding="18"
	app:headerColumnTextColor="#8f000000"
	app:headerRowPadding="30"
	app:columnGap="24"
	app:noOfVisibleDays="3"
	app:headerRowBackgroundColor="#efefef"
	app:dayBackgroundColor="#05000000"
	app:todayBackgroundColor="#1848adff"
	app:headerColumnBackground="#ffffffff"
	app:todayHeaderTextColor="#2789e4" />
```
完整调用:
```
	// Show a toast message about the touched event.
	mWeekView.setOnEventClickListener(this);
	// The week view has infinite scrolling horizontally. We have to provide the events of a
	// month every time the month changes on the week view.
	mWeekView.setMonthChangeListener(mMonthChangeListener);
	// Set long press listener for events.
	mWeekView1.setEventLongPressListener(this);
	// Set long press listener for empty view
	mWeekView.setEmptyViewLongPressListener(this);
	
	MonthLoader.MonthChangeListener mMonthChangeListener = new MonthLoader.MonthChangeListener() {
    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        // Populate the week view with some events.
        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();

        Calendar startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 3);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, newMonth-1);
        startTime.set(Calendar.YEAR, newYear);
        Calendar endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR, 1);
        endTime.set(Calendar.MONTH, newMonth-1);
        WeekViewEvent event = new WeekViewEvent(1, getEventTitle(startTime), startTime, endTime);
        event.setColor(getColor(ResourceTable.Color_event_color_01));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 3);
        startTime.set(Calendar.MINUTE, 30);
        startTime.set(Calendar.MONTH, newMonth-1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.set(Calendar.HOUR_OF_DAY, 4);
        endTime.set(Calendar.MINUTE, 30);
        endTime.set(Calendar.MONTH, newMonth-1);
        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
        event.setColor(getColor(ResourceTable.Color_event_color_02));
        events.add(event);

        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 4);
        startTime.set(Calendar.MINUTE, 20);
        startTime.set(Calendar.MONTH, newMonth-1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.set(Calendar.HOUR_OF_DAY, 5);
        endTime.set(Calendar.MINUTE, 0);
        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
        event.setColor(getColor(ResourceTable.Color_event_color_03));
        events.add(event);
 
        return events;
    }
};
```

##### 关于 [WeekView]


| Method                      | Description                      |
| :-------------------------- | :------------------------------- |
| setNumberOfVisibleDays        | 设置视图显示天数				     |
| setColumnGap                  | 设置列间距				         |
| setTextSize                   | 设置title字体大小				  |
| setEventTextSize              | 设置日历内容字体大小				  |
| goToToday                     | 在周视图上显示今天				  |


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.2.7
- 0.0.1-SNAPSHOT


#### 版权和许可信息
``` 
Copyright 2014 Raquib-ul-Alam

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```