package com.alamkanak.weekview.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        setUIContent(ResourceTable.Layout_ability_main);
        Text btn_basic = (Text) findComponentById(ResourceTable.Id_btn_basic);
        Text btn_asyc = (Text) findComponentById(ResourceTable.Id_btn_asyc);

        btn_basic.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.alamkanak.weekview.sample")
                        .withAbilityName("com.alamkanak.weekview.sample.BaseAbility")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);


            }
        });

        btn_asyc.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.alamkanak.weekview.sample")
                        .withAbilityName("com.alamkanak.weekview.sample.AsynchronousAbility")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);

            }
        });
    }




}
