package com.alamkanak.weekview.sample;

import com.alamkanak.weekview.utils.CalendarUtils;
import org.junit.Test;

import java.util.Calendar;

public class ExampleTest {

    @Test
    public void today(){
        Calendar calendar = CalendarUtils.today();
        System.out.println(calendar.getTime().toString());
    }

    @Test
    public void isSameDay(){

        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        boolean status = CalendarUtils.isSameDay(date,CalendarUtils.today());
        System.out.println(status);
    }

    @Test
    public void isNotSameDay(){

        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, -2);
        date.set(Calendar.SECOND, 1);
        date.set(Calendar.MILLISECOND, 0);

        boolean status = CalendarUtils.isSameDay(date,CalendarUtils.today());
        System.out.println(status);
    }

    @Test
    public void beforNow(){
        Calendar calendar = CalendarUtils.today();
        int now = CalendarUtils.beforNow(calendar);
        System.out.println(now);
    }

    @Test
    public void beforNow2(){
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, -2);
        date.set(Calendar.SECOND, 1);
        date.set(Calendar.MILLISECOND, 0);

        int now = CalendarUtils.beforNow(date);
        System.out.println(now);
    }

    @Test
    public void isTimeAfterOrEqualsAfter(){
        Calendar calendar = CalendarUtils.today();
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 2);
        date.set(Calendar.SECOND, 1);
        date.set(Calendar.MILLISECOND, 0);

        boolean status = CalendarUtils.isTimeAfterOrEquals(calendar,date);
        System.out.println(status);
    }

    @Test
    public void isTimeAfterOrEqualsEqual(){
        Calendar calendar = CalendarUtils.today();
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        boolean status = CalendarUtils.isTimeAfterOrEquals(calendar,date);
        System.out.println(status);
    }

    @Test
    public void isTimeAfterOrEqualsBefore2(){
        Calendar calendar = CalendarUtils.today();
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, -2);
        date.set(Calendar.MINUTE, 2);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        boolean status = CalendarUtils.isTimeAfterOrEquals(calendar,date);
        System.out.println(status);
    }

    @Test
    public void isTimeAfterOrEqualsBefore(){
        Calendar calendar = CalendarUtils.today();
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, 2);
        date.set(Calendar.MINUTE, 2);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        boolean status = CalendarUtils.isTimeAfterOrEquals(calendar,date);
        System.out.println(status);
    }

    @Test
    public void getEventTitle(){
        Calendar calendar = CalendarUtils.today();
        String date = CalendarUtils.getEventTitle(calendar);
        System.out.println(date);
    }

}
