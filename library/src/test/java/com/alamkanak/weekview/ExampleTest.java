package com.alamkanak.weekview;

import com.alamkanak.weekview.utils.CalendarUtils;
import org.junit.Test;

import java.util.Calendar;

public class ExampleTest {

    @Test
    public void getEventTitle(){
        Calendar calendar = CalendarUtils.today();
        String date = CalendarUtils.getEventTitle(calendar);
        System.out.println(date);
    }

}
